<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Genero;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Lang;
use Tests\Traits\TestValidation;

class GeneroControllerTest extends TestCase
{
    use DatabaseMigrations, TestValidation;
    
    private $genero;

    protected function setUp(): void
    {
        parent::setUp();
        $this->genero = factory(Genero::class)->create();
    }

    public function testExample()
    {   
        
        $response = $this->get('/');
        $response->assertStatus(200);
    }
    public function testInvalidationData(){ 
        $data = [
            'name' => ''
        ];
        // $response = $this->json('POST', route('categories.store'), []);
        // $this->assertInvalidationRequired($response);
        $this->assertInvalidationInStoreActions($data, 'required');
        $this->assertInvalidationInUpdateAction($data, 'required');
        $data = [
            'name' => str_repeat('a', 260),
        ];

        $this->assertInvalidationInStoreActions($data, 'max.string', ['max' => 255]);
        $this->assertInvalidationInUpdateAction($data, 'max.string', ['max' => 255]);


        // $this->assertInvalidationMax($response);

        $data = [
            'is_active' => "a"
        ];
        $this->assertInvalidationInStoreActions($data, 'boolean');
        // $this->assertInvalidationBoolean($response);
        $this->assertInvalidationInUpdateAction($data, 'boolean');

   
        // $response = $this->json('POST', route('generos.store', []));
        // $this->assertInvalidationRequired($response);
        //  $response = $this->json('POST', route('generos.store'), [
        //      'name'=>str_repeat('a', 260),
        //      'is_active'=>"a"
        //     ]);         
        // $this->assertInvalidationMax($response); 
        // $this->assertInvalidationBoolean($response);    
             
    }
    public function testSyncCategories()
    {
        $categoriesId = factory(Category::class, 3)->create()->pluck('id')->toArray();

        $sendData = [
            'name' => 'test',
            'categories_id' => [$categoriesId[0]]
        ];
        $response = $this->json('POST', $this->routeStore(), $sendData);
        $this->assertDatabaseHas('category_genero', [
            'category_id' => $categoriesId[0],
            'genero_id' => $response->json('data.id')
        ]);

        $sendData = [
            'name' => 'test',
            'categories_id' => [$categoriesId[1], $categoriesId[2]]
        ];
        $response = $this->json(
            'PUT',
            route('genero.update', ['genero' => $response->json('data.id')]),
            $sendData
        );
        $this->assertDatabaseMissing('category_genero', [
            'category_id' => $categoriesId[0],
            'genero_id' => $response->json('data.id')
        ]);
        $this->assertDatabaseHas('category_genero', [
            'category_id' => $categoriesId[1],
            'genero_id' => $response->json('data.id')
        ]);
        $this->assertDatabaseHas('category_genero', [
            'category_id' => $categoriesId[2],
            'genero_id' => $response->json('data.id')
        ]);

    } //test syncCategoryEnd

    protected function assertInvalidationRequired(TestResponse $response){
        $this->assertInvalidationFields($response, ['name'], 'required', []);
        $response->assertJsonMissingValidationErrors('is_active');
    }
    protected function assertInvalidationMax(TestResponse $response){
        $this->assertInvalidationFields($response, ['name'], 'max.string', ['max' => 255]);                  
    }
    protected function assertInvalidationBoolean(TestResponse $response){
        // $this->assertInvalidationFields($response, ['is_active'], 'boolean', []);  
        $response->assertStatus(422)
        ->assertJsonValidationErrors('is_active')
        ->assertJsonFragment([
            Lang::get('validation.boolean', ['attribute' => 'is_active'])
        ]);
                      
    }

    public function testDestroy(){

        $genero = factory(Genero::class)->create();       
        $response = $this->json('DELETE', route('generos.destroy',['genero'=>$genero->id]));        
        $response->assertStatus(204);                  
        $this->assertNull(Genero::find($genero->id)); 
        $this->assertNotNull(Genero::withTrashed()->find($genero->id));               
    } 

    protected function routeStore()
    {
        return route('generos.store');
    }
    protected function routeUpdate()
    {
        return route('generos.update', ['genero' => $this->genero->id]);
    }

    protected function model()
    {
        return Genero::class;
    }
}
