<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Route;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Lang;
use PhpParser\Node\Expr\Cast\String_;
use Tests\TestCase;
use Tests\Traits\TestSaves;
use Tests\Traits\TestValidation;

class CategoryControllerTest extends TestCase
{
    use DatabaseMigrations, TestValidation, TestSaves;

    private $category;


    protected function setUp(): void
    {
        parent::setUp();
        $this->category = factory(Category::class)->create();
    }

    public function testExample()
    {

        $response = $this->get(route('categories.index'));
        // printf($categories);
        $response->assertStatus(200)
            ->assertJson([$this->category->toArray()]);
    }

    public function testShow()
    {

        $response = $this->get(route('categories.show', ['category' => $this->category->id]));
        $response->assertStatus(200)
            ->assertJson($this->category->toArray());
    }

    public function testInvalidationData()
    {
        $data = [
            'name' => ''
        ];
        // $response = $this->json('POST', route('categories.store'), []);
        // $this->assertInvalidationRequired($response);
        $this->assertInvalidationInStoreActions($data, 'required');
        $this->assertInvalidationInUpdateAction($data, 'required');
        $data = [
            'name' => str_repeat('a', 260),
        ];

        $this->assertInvalidationInStoreActions($data, 'max.string', ['max' => 255]);
        $this->assertInvalidationInUpdateAction($data, 'max.string', ['max' => 255]);


        // $this->assertInvalidationMax($response);

        $data = [
            'is_active' => "a"
        ];
        $this->assertInvalidationInStoreActions($data, 'boolean');
        // $this->assertInvalidationBoolean($response);
        $this->assertInvalidationInUpdateAction($data, 'boolean');


        // $category = factory(Category::class)->create();
        // $response = $this->json(
        //     'PUT',
        //     route(
        //         'categories.update',
        //         ['category' => $category->id]
        //     ),
        //     []
        // );
        // $this->assertInvalidationRequired($response);



        // $response = $this->json(
        //     'PUT',
        //     route(
        //         'categories.update',
        //         ['category' => $category->id]
        //     ),
        //     [
        //         'name' => str_repeat('a', 260),
        //         'is_active' => "a"
        //     ]
        // );
        // $this->assertInvalidationMax($response);
        // $this->assertInvalidationBoolean($response);
    }



    protected function assertInvalidationRequired(TestResponse $response)
    {
        $this->assertInvalidationFields($response, ['name'], 'required', []);
        $response->assertJsonMissingValidationErrors('is_active');
    }

    protected function assertInvalidationMax(TestResponse $response)
    {
        $this->assertInvalidationFields($response, ['name'], 'max.string', ['max' => 255]);
    }

    protected function assertInvalidationBoolean(TestResponse $response)
    {
        $this->assertInvalidationFields($response, ['is_active'], 'boolean', []);
    }

    public function testInvalidationDataName()
    {
        $response = $this->json('POST', route('categories.store'), [
            'name' => str_repeat('a', 260)
        ]);

        $this->assertInvalidationMax($response);
    }


    public function testStore()
    {
        $data = ["name" => "test"];
        $response = $this->assertStore($data, $data + ['description' => null, 'is_active' => true, 'deleted_at' => null]);

        $response->assertJsonStructure(['created_at']);

        $data = [

            'name' => 'papelaria',
            'description' => 'materiais',
            'is_active' => false

        ];
        $this->assertStore($data, $data + ['description' => 'materiais', 'is_active' => false]);
        // $response = $this->json(
        //     'POST',
        //     route('categories.store'),
        //     [
        //         'name' => 'papelaria',
        //     ]
        // );

        // $id = $response->json('id');
        // $category = Category::find($id);

        // $response->assertStatus(201)
        //     ->assertJson($category->toArray());
        // $this->assertTrue($response->json('is_active'));
        // $this->assertNull($response->json('description'));

        // $response = $this->json(
        //     'POST',
        //     route('categories.store'),
        //     [
        //         'name' => 'papelaria',
        //         'description' => 'materiais',
        //         'is_active' => false
        //     ]
        // );

        // $id = $response->json('id');
        // $category = Category::find($id);

        // $response->assertStatus(201)
        //     ->assertJson($category->toArray());
        // $this->assertFalse($response->json('is_active'));
        // $this->assertNotNull($response->json('description'));

        // $response->assertJsonFragment([
        //     'description' => 'materiais',
        //     'is_active' => false
        // ]);
    }

    public function testUpdate()
    {
        $this->category = factory(Category::class)->create([
            'description' => 'description',
            'is_active' => false
        ]);
        $data = [
            'name' => 'papelaria',
            'description' => 'materiais',
            'is_active' => false
        ];
        $response = $this->assertUpdate(
            $data, 
            $data + ['deleted_at'=> null],             
        );
        $response->assertJsonStructure(['created_at']);
        // $response = $this->json(
        //     'PUT',
        //     route(
        //         'categories.update',
        //         ['category' => $this->category->id]
        //     ),
        //     [
        //         'name' => 'papelaria',
        //         'description' => 'materiais',
        //         'is_active' => false
        //     ]
        // );

        // $id = $response->json('id');
        // $category = Category::find($id);

        // $response->assertStatus(200)
        //     ->assertJson($category->toArray());
        // $this->assertFalse($response->json('is_active'));
        // $this->assertNotNull($response->json('description'));

        // $response->assertJsonFragment([
        //     'description' => 'materiais',
        //     'is_active' => false
        // ]);

        // $response = $this->json(
        //     'PUT',
        //     route(
        //         'categories.update',
        //         ['category' => $category->id]
        //     ),
        //     [
        //         'name' => 'papelaria',
        //         'description' => '',
        //     ]
        // );

        // $response->assertJsonFragment([
        //     'description' => null,
        // ]);

        // $category->description = 'teste';
        // $category->save();
        // $response = $this->json(
        //     'PUT',
        //     route(
        //         'categories.update',
        //         ['category' => $category->id]
        //     ),
        //     [
        //         'name' => 'papelaria',
        //         'description' => null,
        //     ]
        // );
        // $response->assertJsonFragment([
        //     'description' => null,
        // ]);
    }
    public function testDestroy()
    {

        $response = $this->json('DELETE', route('categories.destroy', ['category' => $this->category->id]));
        $response->assertStatus(204);
        $this->assertNull(Category::find($this->category->id));
        $this->assertNotNull(Category::withTrashed()->find($this->category->id));
    }

    protected function routeStore()
    {
        return route('categories.store');
    }
    protected function routeUpdate()
    {
        return route('categories.update', ['category' => $this->category->id]);
    }

    protected function model()
    {
        return Category::class;
    }
}///class end
