<?php

namespace Tests\Feature\Models;

use App\Models\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    public function testList()
    {
        // Category::create([
        //     'name' => 'teste1'
        // ]);
        factory(Category::class, 1)->create();
        $categories = Category::all();
        $categoryKeys = array_keys($categories->first()->getAttributes());

        $this->assertNotEmpty($categories);
        $this->assertCount(1, $categories);
        $this->assertEqualsCanonicalizing(
            [
                "id",
                "name",
                "description",
                "is_active",
                "deleted_at",
                "created_at",
                "updated_at"
            ],
            $categoryKeys
        );
    }

    public function testCreate(){
        $category = Category::create(
            [
                'name' => 'test1',
            ]
        );
        print_r($category->is_active);
        $category->refresh();
        $this->assertEquals(36, strlen($category->id));
        $this->assertEquals('test1', $category->name);
        $this->assertNull($category->description);
        $this->assertTrue($category->is_active);

        $category = Category::create(
            [
                'name' => 'test1',
                'description' => null,
            ]
        );

        $this->assertNull($category->description);

        $category = Category::create(
            [
                'name' => 'test1',
                'description' => 'test_description',
            ]
        );

        $this->assertEquals('test_description', $category->description);
    }

    public function testUpdate(){

        $category = factory(Category::class)->create(
            [ 
                'description' => 'test_description',
                'is_active' => false
            ]
        );

        $category->update(
            [
                'name' => 'test2',
                'description' => 'test_description2',
                'is_active' => true
            ]
        );

        $this->assertEquals('test2', $category->name);
        // $this->assertNull($category->description);
        $this->assertTrue($category->is_active);       
        $this->assertEquals('test_description2', $category->description);
    }


    public function testDelete(){
        $category = factory(Category::class)->create(
            [ 
                'description' => 'test_description',
                'is_active' => false
            ]
        );        
        $category->delete();
        $this->assertNull(Category::find($category->id));
        $category->restore();
        $this->assertNotNull(Category::find($category->id));

    }
}
