<?php

namespace Tests\Feature\Models;

use App\Models\Genero;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GeneroTest extends TestCase
{
    use DatabaseMigrations;

    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testList()
    {
        factory(Genero::class, 1)->create();
        $generos = Genero::all();
        $generosKeys = array_keys($generos->first()->getAttributes());
        $this->assertNotEmpty($generos);
        $this->assertCount(1, $generos);
        $this->assertEqualsCanonicalizing(
            [
                "id",
                "name",                
                "is_active",
                "deleted_at",
                "created_at",
                "updated_at"
            ],
            $generosKeys
        );
    }

    public function testCreate(){
        $genero = Genero::create(
            [
                'name' => 'test1',
                'is_active' => true
            ]
        );        
       
        // $this->assertEquals(36, strlen($genero->id));
        $this->assertEquals('test1', $genero->name);       
        $this->assertTrue($genero->is_active);
                  
    }

}
