<?php

namespace Tests\Models\Unit;

use App\Models\Category;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Uuid;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    

    public function testFillable()
    {
        //$this->assertTrue(true);
        $category = new Category();       
        $fillable = ['name', 'description', 'is_active'];
        $this->assertEquals($fillable, $category->getFillable());
    }

    public function testIfUseTraits()
    {
        $traits = [
            SoftDeletes::class, Uuid::class
        ];
        $categoryTraits = array_keys(class_uses(Category::class));
        // print_r($traits);
        // print_r(class_uses(Category::class));
        $this->assertEquals($traits, $categoryTraits);
    }

    public function testCast()
    {
        //$this->assertTrue(true);
        $category = new Category();
        $cast = ['id' => 'string', 'is_active' => 'boolean'];
        $this->assertEquals($cast, $category->getCasts());
    }

    public function testDate()
    {
        //$this->assertTrue(true);
        $category = new Category();
        $dates = ['deleted_at', 'created_at', 'updated_at'];
        // $this->assertEquals($dates, $category->getDates());
        foreach ($dates as $date) {
            $this->assertContains($date, $category->getDates());
        }

        $this->assertCount(count($dates), $category->getDates());
    }
}
