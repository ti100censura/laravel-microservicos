<?php

declare(strict_types=1);

namespace tests\Traits;

use Exception;
use Illuminate\Foundation\Testing\TestResponse;

trait TestSaves
{
    protected abstract function model();
    protected function assertStore(array $sentdata, array $testsdata, array $testJsondata = null)
    {
        /** @var TestResponse $response*/ 
        $response = $this->json(
            'POST',
            $this->routeStore(),
            $sentdata
        );
        $this->assertInDatabase($response, $testsdata);
        if($response->status() != 201){
            throw new Exception("Response must be 201, given {$response->status()}:\n {$response->content()}");
        }
       
        $testResponse = $testJsondata ?? $testsdata;
        $response->assertJsonFragment($testResponse + ['id'=>$response->json('id')]);
        return $response;
    }
    protected function assertUpdate(array $sentdata, array $testsdata, array $testJsondata = null):TestResponse
    {   
        /** @var TestResponse $response*/ 
        $response = $this->json(
            'PUT',
            $this->routeUpdate(),
            $sentdata
        );  
        $this->assertInDatabase($response, $testsdata);
        if($response->status() != 200){
            throw new Exception("Response must be 200, given {$response->status()}:\n {$response->content()}");
        }
        $this->assertJsonResponseContent($response, $testsdata, $testJsondata);
        return $response;
    }

    private function assertInDatabase($response, $testsdata){
        $model = $this->model();
        $table = (new $model)->getTable();
        $this->assertDatabaseHas($table, $testsdata + ['id'=>$response->json('id')]);
    }
    private function assertJsonResponseContent($response, $testsdata, $testJsondata = null){
        $testResponse = $testJsondata ?? $testsdata;
        $response->assertJsonFragment($testResponse + ['id'=>$response->json('id')]);
    }
}
