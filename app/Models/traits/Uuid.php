<?php

namespace App\Models\Traits;
use \Ramsey\Uuid\Uuid as RanseyUiid;

trait Uuid{
    public static function boot(){
        parent::boot();
        static::creating(function($obj){
            $obj->id = RanseyUiid::uuid4()->toString();
        });
    }
}