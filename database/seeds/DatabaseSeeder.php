<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //    factory(\App\Models\Category::class, 100)->create();
        //    factory(\App\Models\Genero::class, 100)->create();
        //    factory(\App\Models\CastMember::class, 100)->create();
        //    factory(\App\Models\Videos::class, 100)->create();

           $this->call(CategoriesTableSeeder::class);          
           $this->call(GenerosTableSeeder::class);
           $this->call(CastMembersTableSeeder::class);
           $this->call(VideosTableSeeder::class);
    }       
}
